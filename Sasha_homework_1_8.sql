/*
Из заданной таблицы 
получите следующую последовательность:
    100
    101
    102
    103
*/

drop table dz1_8;

create table dz1_8 (
       id number(5),
       from_src number(5),
       to_src number(5)
);

insert all into dz1_8
  (id, from_src, to_src)
values
  (1, 100, 101) into dz1_8
  (id, from_src, to_src)
values
  (2, 100, 102) into dz1_8
  (id, from_src, to_src)
values
  (3, 100, 103) into dz1_8
  (id, from_src, to_src)
values
  (4, 101, 102) into dz1_8
  (id, from_src, to_src)
values
  (5, 101, 103) into dz1_8
  (id, from_src, to_src)
values
  (6, 102, 103)
  select 1 from dual;

commit;

select numb from
       (select from_src numb from dz1_8
       union
       select to_src numb from dz1_8)
order by numb;
