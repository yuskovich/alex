select avg(salary),
       department_name
  from (select department_id,
               case
                 when salary > 4000 then
                  salary
                 else
                  2000
               end salary
          from employees)
  join departments
 using (department_id)
 group by department_id,
          department_name;
