select e.last_name,
       e.job_id,
       e.salary
  from employees e
 where e.salary in (select min(salary) from employees group by department_id)
