select count(employee_id),
       max(salary),
       d.department_name
  from employees e
  join departments d
 using (department_id)
 where d.department_name like 'Pu%'
 group by department_id,
          d.department_name
