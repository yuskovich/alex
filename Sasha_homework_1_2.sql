/*
2. Есть столбец, содержащий следующие значения:
  100
   96
   500
   987
   799
   80
   300
   657
Исключите значения, которые передаются строкой в запрос -  ’96,300,799’
*/

drop table DZ1_2;

create table DZ1_2
( samplevalues number (5)
);

insert all
       into DZ1_2 (Samplevalues) values (100)
       into DZ1_2 (Samplevalues) values (96)
       into DZ1_2 (Samplevalues) values (500)
       into DZ1_2 (Samplevalues) values (987)
       into DZ1_2 (Samplevalues) values (799)
       into DZ1_2 (Samplevalues) values (80)
       into DZ1_2 (Samplevalues) values (300)       
       into DZ1_2 (Samplevalues) values (657)
select * from dual;
commit;

/*
select * 
from DZ1_2
WHERE SAMPLEVALUES NOT IN(
                  select regexp_substr('96,300,799','[^,]+', 1, level)
                  from dual
                  connect by regexp_substr('96,300,799', '[^,]+', 1, level) is not null);
*/

select *
from DZ1_2
WHERE instr ('96,300,799', to_char(samplevalues)) = 0;
