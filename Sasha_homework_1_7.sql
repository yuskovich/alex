/*
Есть таблица источник GRATH_SRC и TEMP_GRATH
В результат запроса должны попасть новые записи, удаленные и измененные только по полю weight_src
*/


/*drop table grath_src;
create table grath_src (
id number(5),
from_src number(5),
to_src number(5),
weight_src number(5)
);

insert all
into grath_src (id, from_src, to_src, weight_src) values (1,100,102,100)
into grath_src (id, from_src, to_src, weight_src) values (2,100,101,400)
into grath_src (id, from_src, to_src, weight_src) values (3,101,102,200)
into grath_src (id, from_src, to_src, weight_src) values (4,300,301,300)
into grath_src (id, from_src, to_src, weight_src) values (5,300,302,400)
into grath_src (id, from_src, to_src, weight_src) values (6,301,302,500)
select 1 from dual;
commit;

drop table temp_grath;
create table temp_grath (
from_temp number(5),
to_temp number(5),
weight number(5)
);

insert all
into temp_grath (from_temp, to_temp, weight) values (100,101,200)
into temp_grath (from_temp, to_temp, weight) values (200,201,200)
into temp_grath (from_temp, to_temp, weight) values (300,301,300)
into temp_grath (from_temp, to_temp, weight) values (300,302,400)
into temp_grath (from_temp, to_temp, weight) values (301,302,600)
select 1 from dual;
commit;

select * from grath_src;
select * from temp_grath;
*/

(     select
       from_src
       ,to_src
       ,weight_src
       from grath_src
) 
UNION
(
   select 
       from_temp
       ,to_temp
       ,weight
   from temp_grath  
) 
MINUS
(
  (select
       from_src
       ,to_src
       ,weight_src
   from grath_src) 
INTERSECT
   (select 
        from_temp
        ,to_temp
        ,weight
        from temp_grath) 
);

select id, from_src, to_src, weight_src, from_temp, to_temp, weight
from grath_src g
full outer join temp_grath t on
     g.from_src = t.from_temp AND
     g.to_src = t.to_temp AND
     g.weight_src = t.weight
where coalesce (from_src, to_src, weight_src) is null or
coalesce (from_temp, to_temp, weight) is null;
